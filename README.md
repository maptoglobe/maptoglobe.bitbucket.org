# "Map to Globe" - by /u/notcaffeinefree

### General Image Information ###
For any image you use, if it looks grainy it's simply too small. The higher resolution image you use, the crisper it will look on the globe.

### Heightmap Information ###
**IMPORTANT:** When using a heightmap image, it should be the same resolution as the main map image.

Using a heightmap will add terrain texture to your map. To use a heightmap, you must have a greyscale image of your map. The greyscale image should use white for high terrain and black for low. It doesn't have to be absolute black and white though. The starting at completely black ("sea level"), the whiter a pixel is the higher the elevation. For more information, refer to [the Wikipedia page](https://en.wikipedia.org/wiki/Heightmap). For example, the Earth's heightmap looks like [this](http://i.imgur.com/6H0aWqf.jpg).

There is an issue with the lighting when using the 3D heightmap option, which I am trying to fix, though there's no guarantee that will happen. You can still use the right mouse button to move the light around, but it can be tricky to place it where you want it to be.

### Saving ###
Clicking the "*Save*" button will upload the image(s) to imgur and then use the part of the resulting imgur URL to give you a specific URL to your globe setup so that you can access it later on (and share it). As an example, if the imgur URL for the map image is "http://i.imgur.com/**rygRDXr**.jpg" and the background is "http://i.imgur.com/**Ep7m961**.jpg", then the URL for your globe will be "maptoglobe.bitbucket.com?map=**rygRDXr**&bg=**Ep7m961**". Notice the matching bolded values (really, you can use this to put any imgur image into the globe system).

Clicking the "*Upload screenshot to imgur*" button does just that. It takes a picture of the whole browser window and uploads the image automatically to imgur. Right now, it uploads at whatever size your browser window is at when you hit the button. If you have a large monitor, you may end up with a large image so you may want to scale your browser window down before clicking the button.

### "My map looks weird" - A note on map projections ###
When converting from a 2d image to sphere or sphere to 2d, there will always be distortion. There's a whole Wikipedia article on [map projections](https://en.wikipedia.org/wiki/Map_projection) that goes over this. From what I can tell, the method that this code uses to wrap a 2d image around the sphere works best with a map following the [Equirectangular projection](https://en.wikipedia.org/wiki/Equirectangular_projection). Take note how that projection distorts the image around the poles and take this into consideration when using this app with any maps you have made (that is, if you haven't drawn your map with the distortion of a equirectangular projection in mind, then you may end up with weird looking poles on the sphere). If you use a map that does not follow this projection (example: a square image), it will be distorted incorrectly.

While this app allows you to look at different map projections, it still requires that the original image be equirectangular (or at least a 2:1 image ratio). Again, if you use something different it will distort incorrectly. If you have a map that uses a different projection, you can use a program like [GProjector](http://www.giss.nasa.gov/tools/gprojector/) to convert it.

### Errors trying to "upload a screenshot" or "save"? ###

Both "saving" and "uploading a screenshot" use imgur. To "save", the map image (and background image, if you used one) must be under the 10MB limit imposed by imgur. If you add an image that's over 10MB, the "save" button will be disabled. If you see "Error uploading", then there was a problem on imgur's end. It could be that imgur is down and/or not responding, a problem with the image you're trying to upload, or something else. If you can manually navigate to imgur, yet you still see this error message, let me know.

### Performance Issues? ###
To allow for screenshots, certain code had to be used that may impact performance. If it's really bad for you, please send [/u/notcaffeinefree a message](http://reddit.com/message/compose/?to=notcaffeinefree) (and/or try another browser). In my testing, Firefox appeared to be the worst, performance wise.

### Internet Explorer Issues ###
Yes, IE has problems. I've tested this using IE11 and it still doesn't perform as well, in some areas, as FF and Chrome. Using a heightmap will through an error message in the console (though it doesn't appear to stop the heightmap from working completely). The lighting in the 3D heightmap option is completely messed up. From what I've found elsewhere online, these are caused by problems on IE's end. Lastly, IE does not support the [method](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a) I've used to download screenshots.

### Other Problems? ###
Unfortunately the only QA for this is myself and those who use this. If you find problems, please send me a message and I'll try to fix them.